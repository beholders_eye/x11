# Copyright 2016-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    require gitlab [ prefix=https://gitlab.freedesktop.org user=virgl ]
else
    require gitlab [ prefix=https://gitlab.freedesktop.org user=virgl tag=${PV} ]
fi

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="Virgl rendering library"
DESCRIPTION="
The virgil3d rendering library is used by QEMU to implement 3D GPU support for the virtio GPU.
"
HOMEPAGE+=" https://virgil3d.github.io"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="X"

# Tests try to access /dev/dri/* resulting in access violations in sydbox
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/python:*
        sys-devel/autoconf-archive
        virtual/pkg-config
        x11-utils/util-macros[>=1.8]
    build+run:
        dev-libs/libepoxy[X?]
        x11-dri/libdrm[>=2.4.50]
        x11-dri/mesa [[ note = [ gbm ] ]]
        X? ( x11-libs/libX11 )
    test:
        dev-libs/check[>=0.9.4]
"

! ever is_scm && WORK=${WORKBASE}/${PNV}-48cc96c9aebb9d0164830a157efc8916f08f00c0

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-egl
    --enable-gbm-allocation
    --disable-code-coverage
    --disable-fuzzer
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'X glx'
)
DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-tests --disable-tests'
)

